<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSytesaPlantSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sytesa_plant_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('maxPal')->nullable()->default(500);
            $table->double('minPal')->nullable()->default(400);
            $table->double('maxMag')->nullable()->default(500);
            $table->double('minMag')->nullable()->default(400);
            $table->double('maxTemperature')->nullable()->default(40);
            $table->double('minTemperature')->nullable()->default(35);
            $table->integer('maxGate')->nullable()->default(1);
            $table->integer('minGate')->nullable()->default(0);
            $table->integer('maxC1')->nullable()->default(3);
            $table->integer('minC1')->nullable()->default(0);
            $table->integer('maxC2')->nullable()->default(3);
            $table->integer('minC2')->nullable()->default(0);
            $table->integer('maxC3')->nullable()->default(3);
            $table->integer('minC3')->nullable()->default(0);
            $table->double('maxP1')->nullable()->default(80);
            $table->double('minP1')->nullable()->default(50);
            $table->double('maxp2')->nullable()->default(80);
            $table->double('minp2')->nullable()->default(50);
            $table->double('maxp3')->nullable()->default(80);
            $table->double('minp3')->nullable()->default(50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sytesa_plant_settings');
    }
}
