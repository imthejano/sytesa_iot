<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettingsToSytesaPlants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sytesa_plants', function (Blueprint $table) {
            $table->double('latitude')->nullable()->default(19.4658107);
            $table->double('longitude')->nullable()->default(-99.2235188);
            $table->bigInteger('settings_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sytesa_plants', function (Blueprint $table) {
            //
        });
    }
}
