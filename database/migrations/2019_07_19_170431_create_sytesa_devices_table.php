<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSytesaDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sytesa_devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sigfox_id', 10);
            $table->string('sigfox_id_hash', 100);
            $table->integer('type');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sytesa_devices');
    }
}
