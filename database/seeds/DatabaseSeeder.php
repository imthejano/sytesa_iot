<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // for($i=1;$i<5;$i++){
        //     $region=new App\Region;
        //     $region->name='region'.$i;
        //     $region->save();
        // }
        // $this->call(PlantsTableSeeder::class);
        // // $this->call(UsersTableSeeder::class);
        // for($i=25;$i<25+45;$i++){
        //     $user=App\User::find($i);
        //     $role=App\Role::find(8);
		//     $user->roles()->attach($role);
        // }
        // $users=App\User::where('id','>',25)->get();
        // foreach ($users as $key => $user) {
        //     $region=App\Region::find(rand(1,3));
		//     $user->region()->attach($region);
        // }

        // $plants=App\Plant::all();
        // foreach ($plants as $key => $plant) {
        //     $device=new App\Device;
        //     $device->sigfox_id='syt'.$plant->id;
        //     $device->sigfox_id_hash=Hash::make('syt'.$plant->id);
        //     $device->type='1';
        //     $device->status='3';
        //     $device->save();
        //     $plant->device_id=$device->id;
        //     $plant->save();
        // }
        
        // $plants=App\Plant::all();
        // foreach ($plants as $key => $plant) {
        //     $setting= new App\SytesaPlantSetting;
        //     $setting->save();
        //     $plant->settings_id=$setting->id;
        //     $plant->save();
        // }


        $devices=App\Device::all();
        foreach ($devices as $key => $device) {
            $nDays=5;
            $interval=10;
            $date=Carbon::Now();
            $date=$date->subDays($nDays);
            for ($i=0; $i < ($nDays*24*60)/$interval; $i++) { 
                $mag=rand(0,500);
                $pal=rand(0,500);
                $c1=rand(0,3);
                $c2=rand(0,3);
                $c3=rand(0,3);
                $gate=rand(0,1);
                $p1=rand(0,100);
                $p2=rand(0,100);
                $p3=rand(0,100);
                $data=new App\Data;
                $data->timestamps=null;
                $temperature=rand(30,40);
                $data->mag=rand(0,500);
                $data->pal=rand(0,500);
                $data->c1=rand(0,3);
                $data->c2=rand(0,3);
                $data->c3=rand(0,3);
                $data->gate=rand(0,1);
                $data->p1=rand(0,100);
                $data->p2=rand(0,100);
                $data->p3=rand(0,100);
                $data->temperature=rand(30,40);
                $data->device=$device->sigfox_id;
                $str='';
                $str=$str.sprintf('%04X',$mag);
                $str=$str.sprintf('%04X',$pal);
                $strC1=sprintf('%02b',$c1);
                $strC2=sprintf('%02b',$c2);
                $strC3=sprintf('%02b',$c3);
                $strGate=sprintf('%01b',$gate);
                $strAux=$strC1.$strC2.$strC3.$strGate.'0';
                $strAux=bindec($strAux);
                $str=$str.sprintf('%02X',$strAux);
                $strP1=sprintf('%06b',$p1);
                $strP2=sprintf('%06b',$p2);
                $strP3=sprintf('%06b',$p3);
                $strTemperature=sprintf('%06b',$temperature);
                $strAux=$strP1.$strP2.$strP3.$strTemperature;
                $strAux=bindec($strAux);
                $str=$str.sprintf('%06X',$strAux);
                $data->data=$str;
                $date=$date->addMinutes(10);
                $data->timeSys=$date->toDateTimeString();
                $data->save();
            }
        }
    }
}
