<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Plant;
use Faker\Generator as Faker;

$factory->define(Plant::class, function (Faker $faker) {
    $i=1;
    return [
        'name'=>'planta'.$i++,
        'address'=>$faker->streetAddress,
        'region_id'=>rand(1,4),
    ];
});
