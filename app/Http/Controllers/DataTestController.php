<?php

namespace App\Http\Controllers;

use App\DataTest;
use App\Log;
use Illuminate\Http\Request;

class DataTestController extends Controller
{


    public function insert(Request $request){
        $log=new Log;
        $log->log='insercion de dato';
        $log->save();
        $row=new DataTest;
        $row->time=$request->input('time');
        $row->device=$request->input('device');
        $row->data=$request->input('data');
        $row->save();
    }


    public function get(Request $request){
        $data=DataTest::all();
        $response['data']=$data;
        return $response;
    }

    public function getLogs(){
        $logs=Log::all();
        return $logs;
    }
}
