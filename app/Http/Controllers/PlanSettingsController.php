<?php

namespace App\Http\Controllers;

use App\PlanSettings;
use App\Plant;
use Illuminate\Http\Request;

class PlanSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlanSettings  $planSettings
     * @return \Illuminate\Http\Response
     */
    public function show(PlanSettings $planSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlanSettings  $planSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(PlanSettings $planSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlanSettings  $planSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $toJson=$request->input('toJson',true);
        $plant=$request->input('plant');
        $findBy=$request->input('findBy','name');
        $set['maxMag']=$request->input('set_maxMag');
        $set['minMag']=$request->input('set_minMag');

        $plant=Plant::where($findBy,$plant)->first();
        $plant->settings=$plant->settings()->first();
        foreach ($set as $setting => $val) {
            if($val!=null)
                $plant->settings[$setting]=$val;
        }
        $plant->settings->update();
        if($toJson)return json_encode($plant);
        else return $plant;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlanSettings  $planSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanSettings $planSettings)
    {
        //
    }
}
