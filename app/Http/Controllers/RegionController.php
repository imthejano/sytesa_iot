<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Plant;
use App\Supervisor;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$region){
        $toJson=$request->input('toJson',true);
        $withSupervisors=$request->input('with_supervisors',false);
        $withSupervisorsPlants=$request->input('with_supervisors_plants',false);
        $withPlants=$request->input('with_plants',false);
        $findBy=$request->input('findBy','name');
        $region=Region::where($findBy,$region);
        $region=$region->first();
        if($withSupervisors){
            $region->supervisors=$region->supervisors()->get();
            if($withSupervisorsPlants){
                foreach ($region->supervisors as $key => $supervisor) {
                    $supervisor->plants=$supervisor->plants()->get();
                }
            }
        }
        if($withPlants){
            $plants=Plant::where('region_id',$region->id);
            $region->plants=$plants->get();
        }
        $response['region']=$region;
        if($toJson)return json_encode($response);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function get(Request $request){
        $toJson=$request->input('toJson',true);
        $whereName=$request->input('where_name');
        $withPlants=$request->input('with_plants',false);
        $withSupervisors=$request->input('with_supervisors',false);
        $withSupervisorsPlants=$request->input('with_supervisors_plants',false);
        $regions=Region::select('*');
        if($whereName!=null)$regions=$regions->where('name',$whereName);
        $regions=$regions->get();
        if($withSupervisors){
            foreach ($regions as $key => $region) {
                $region->supervisors=$region->supervisors()->get();
                if($withSupervisorsPlants){
                    foreach ($region->supervisors as $key2 => $supervisor) {
                        $supervisor->plants=$supervisor->plants()->get();
                    }
                }
            }
        }
        if($withPlants){
            foreach ($regions as $key => $region) {
                $plants=Plant::where('region_id',$region->id);
                $region->plants=$plants->get();
            }
        }
        $response['regions']=$regions;
        if($toJson)return json_encode($response);
        return $response;
    }
}
