<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use App\Role;
use App\Region;

class AuthController extends Controller{
    public function authenticate(Request $request){
        $user=$request->input('user');
        $password=$request->input('password');
        $remember=$request->input('remember',0);
        $email=filter_var($user, FILTER_VALIDATE_EMAIL);
        if($email==false)$loginMode='userName';
        else $loginMode='email';
        $response['remember']=$remember;
        if($user==null || $password==null){
            if($user==null)$response['erros']['user']="No se lleno el campo usuario";
            if($password==null)$response['erros']['password']="No se lleno el campo password";
            $response['success']=false;
        }else{
            $_user=User::where($loginMode,$user)->first();
            if($_user!=null){
                $roles=$_user->roles()->get();
                $region=$_user->regions()->first();
                $response['region']=$region;
                $credentials=array(
                    $loginMode=>$user,
                    'password'=>$password
                );
                foreach ($roles as $key => $role) {
                    if($role->id==1){
                        //superAdmin
                        if(Auth::attempt($credentials,$remember)){
                            $response['success']=true;
                            $response['message']='logeo correcto';
                            $response['role']='superAdmin';
                            $response['userId']=$_user->id;
                            $response['userName']=$_user->userName;
                        }else{
                        $response['success']=false;
                        $response['errors']['credentials']='Las credenciales son incorrectas';
                        }
                    }
                    if($role->id==7){
                        //admin
                        if(Auth::attempt($credentials,$remember)){
                            $response['success']=true;
                            $response['message']='logeo correcto';
                            $response['role']='admin';
                            $response['userId']=$_user->id;
                            $response['userName']=$_user->userName;
                        }else{
                        $response['success']=false;
                        $response['errors']['credentials']='Las credenciales son incorrectas';
                        }
                    }
                    if($role->id==8){
                        //supervisor
                        if(Auth::attempt($credentials,$remember)){
                            $response['success']=true;
                            $response['message']='Logeo correcto';
                            $response['role']='supervisor';
                            $response['userId']=$_user->id;
                            $response['userName']=$_user->userName;
                            $response['region']=$region;
                        }else{
                        $response['success']=false;
                        $response['errors']['credentials']='Las credenciales son incorrectas';
                        }
                    }
                }
            }else{
                $response['success']=false;
                $response['errors']['credentials']='Las credenciales son incorrectas';
            }
        }
        return json_encode($response);
    }
}
