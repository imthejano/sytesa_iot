<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;
use DB;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function getString(Request $request){
		$mag=$request->input('mag');
		$pal=$request->input('pal');
		$c1=$request->input('c1');
		$c2=$request->input('c2');
		$c3=$request->input('c3');
		$gate=$request->input('gate');
		$p1=$request->input('p1');
		$p2=$request->input('p2');
		$p3=$request->input('p3');
		$temperature=$request->input('temperature');
		$str='';
		$str=$str.sprintf('%04X',$mag);
		$str=$str.sprintf('%04X',$pal);
		$strC1=sprintf('%02b',$c1);
		$strC2=sprintf('%02b',$c2);
		$strC3=sprintf('%02b',$c3);
		$strGate=sprintf('%01b',$gate);
		$strAux=$strC1.$strC2.$strC3.$strGate.'0';
		$strAux=bindec($strAux);
		$str=$str.sprintf('%02X',$strAux);
		$strP1=sprintf('%06b',$p1);
		$strP2=sprintf('%06b',$p2);
		$strP3=sprintf('%06b',$p3);
		$strTemperature=sprintf('%06b',$temperature);
		$strAux=$strP1.$strP2.$strP3.$strTemperature;
		$strAux=bindec($strAux);
		$str=$str.sprintf('%06X',$strAux);
		return $str;
	}
	public function store(Request $request)
	{
		$data=$request->input('data');
		$sigfox_id=$request->input('sigfox_id','4100B8');
		$device=Device::where('sigfox_id',$sigfox_id)->first();
		$timesys=$request->input('timesys');
		if($device!=null){
			$mag=substr($data, 0, 4);
			$mag=base_convert($mag,16,10);
			$pal=substr($data, 4, 4);
			$pal=base_convert($pal,16,10);
			$subStrAux=substr($data,8,2);
			$subStrAux=sprintf( "%08b",base_convert($subStrAux,16,10));
			$c1=substr($subStrAux,0,2);
			$c1=base_convert($c1,2,10);
			$c2=substr($subStrAux,2,2);
			$c2=base_convert($c2,2,10);
			$c3=substr($subStrAux,4,2);
			$c3=base_convert($c3,2,10);
			$gate=substr($subStrAux,6,1);
			$subStrAux=substr($data,10,6);
			$subStrAux=sprintf( "%024b",base_convert($subStrAux,16,10));
			$p1=substr($subStrAux,0,6);
			$p1=base_convert($p1,2,10);
			$p2=substr($subStrAux,6,6);
			$p2=base_convert($p2,2,10);
			$p3=substr($subStrAux,12,6);
			$p3=base_convert($p3,2,10);
			$temperature=substr($subStrAux,18,6);
			$temperature=base_convert($temperature,2,10);
	
			$row=new Sytesa();
			if($sigfox_id!=null)$row->device=$sigfox_id;
			$row->data=$data;
			$row->pal=$pal;
			$row->mag=$mag;
			$row->c1=$c1;
			$row->c2=$c2;
			$row->c3=$c3;
			$row->gate=$gate;
			$row->p1=$p1;
			$row->p2=$p2;
			$row->p3=$p3;
			$row->temperature=$temperature;
			$row->timesys=date("Y-m-d H:i:s");
			if($timesys!=null)$row->timesys=$timesys;
			$row->timestamps=false;
			$row->save();
			return $row;
		}
	}


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function get(Request $request){
		$from=$request->input('from');
        $to=$request->input('to');
        $device=$request->input('device');
		$toJson=$request->input('toJson',false);
		$rows=Data::select(DB::raw('data,device,FORMAT(mag,2) as mag, FORMAT(pal,2) as pal, c1,c2,c3,gate,p1,p2,p3,temperature, date(timesys) as date, dayofweek(timesys) as dayofweek,  month(timesys) as month, year(timesys) as year, time(timesys) as time, timesys as datetime'));
		if($from!=null)$rows=$rows->where('timesys','>=',$from);
        if($to!=null)$rows=$rows->where('timesys','<=',$to);
		if($device!=null)$rows=$rows->where('device',$device);
		$rows=$rows->orderBy('timeSys','desc');
		$rows=$rows->get();
		$response['sytesa_data']=$rows;
		if($toJson)return json_encode($response);
		else return $response;
	}
}
