<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegFxE;
use App\RegNivel;
use App\SytesaData;

class ParserController extends Controller{
	
	public function parseAndStore(Request $request){
		$seqNumber=$request->input('seqNumber');
		$timeUnix=$request->input('time');
        $device=$request->input('device');
		$data=$request->input('data');
		$flag=substr($data,-2,2);
		switch($flag){
			case '00':
				$row=$this->parser0($device,$data,$timeUnix,$seqNumber);
				break;
			case '01':
				$row=$this->parser1($device,$data,$timeUnix,$seqNumber);
				break;
			case '02':
				break;
			case '03':
				break;
			case '04':
				break;
			case '05':
				break;
			case '06':
				break;
			case '07':
				$row=$this->parser7($device,$data,$timeUnix,$seqNumber);
				break;
			case '08':
				break;
			case '09':
				$row=$this->parser9($device,$data,$timeUnix,$seqNumber);
				break;
			case '11':
				break;
			case '12':
				break;
			default:
				break;
		}
		$row->save();
		return $row;
	}

	private function parser0($device,$data,$time,$seqNumber){
		$row=new RegFxE;
		$row->timestamps=false;
		$row->device=$device;
		$row->seqNumber=$seqNumber;
		$row->data=$data;
		$row->timefox = date('Y-m-d H:i:s', $time);
		$row->unix=$time;
		$row->data1 =number_format((hexdec(substr($data, 0,2)) / 20), 2, '.', '');
		$row->data2 =number_format((hexdec(substr($data, 2,2)) / 20), 2, '.', '');
		$row->data3 =number_format((hexdec(substr($data, 4,2)) / 20), 2, '.', '');
		$row->data4 =number_format((hexdec(substr($data, 6,2)) / 20), 2, '.', '');
		$row->data5 =number_format((hexdec(substr($data, 8,2)) / 20), 2, '.', '');
		$row->data6 =number_format((hexdec(substr($data, 10,2)) / 20), 2, '.', '');
		$row->data7 =number_format((hexdec(substr($data, 12,2)) / 20), 2, '.', '');
		$row->data8 =number_format((hexdec(substr($data, 14,2)) / 20), 2, '.', '');
		$row->data9 =number_format((hexdec(substr($data, 16,2)) / 20), 2, '.', '');
		$row->data10 =number_format((hexdec(substr($data, 18,2)) / 20), 2, '.', '');
		$row->data11 =number_format((hexdec(substr($data, 20,2)) / 20), 2, '.', '');
		$row->data12 =number_format((hexdec(substr($data, 22,2)) / 20), 2, '.', '');;
		$row->consumoTxe=0;
		$row->total_eve=0;
		if($row->data1>0){$row->consumoTxe+=$row->data1; $row->total_eve++;}
		if($row->data2>0){$row->consumoTxe+=$row->data2; $row->total_eve++;}
		if($row->data3>0){$row->consumoTxe+=$row->data3; $row->total_eve++;}
		if($row->data4>0){$row->consumoTxe+=$row->data4; $row->total_eve++;}
		if($row->data5>0){$row->consumoTxe+=$row->data5; $row->total_eve++;}
		if($row->data6>0){$row->consumoTxe+=$row->data6; $row->total_eve++;}
		if($row->data7>0){$row->consumoTxe+=$row->data7; $row->total_eve++;}
		if($row->data8>0){$row->consumoTxe+=$row->data8; $row->total_eve++;}
		if($row->data9>0){$row->consumoTxe+=$row->data9; $row->total_eve++;}
		if($row->data10>0){$row->consumoTxe+=$row->data10; $row->total_eve++;}
		if($row->data11>0){$row->consumoTxe+=$row->data11; $row->total_eve++;}
		if($row->data12>0){$row->consumoTxe+=$row->data12; $row->total_eve++;}
		$row->consumoTxe=number_format($row->consumoTxe,2,'.','');
		$row->total_eve=number_format($row->total_eve,2,'.','');
		return $row;
	}

	private function parser1($device,$data,$time,$seqNumber){
		$row=new RegFxE;
		$row->timestamps=false;
		$row->device=$device;
		$row->seqNumber=$seqNumber;
		$row->data=$data;
		$row->timefox = date('Y-m-d H:i:s', $time);
		$row->unix=$time;
		//Resolucion
		$res_ev= 2.55;
		//msk
		$mask_m1_5=0b00000011;
		$mask_m2_6=0b00001100;
		$mask_m3_7=0b00110000;
		$mask_m4_8=0b11000000;
		// Hexa a decimal, dividir data en parejas. aparte transforma en decilitros a litros
		$data1 = (hexdec(substr($data, 0,2))* ($res_ev / 255));
		$data2 = (hexdec(substr($data, 2,2))* ($res_ev / 255));
		$data3 = (hexdec(substr($data, 4,2))* ($res_ev / 255));
		$data4 = (hexdec(substr($data, 6,2))* ($res_ev / 255));
		$data5 = (hexdec(substr($data, 8,2))* ($res_ev / 255));
		$data6 = (hexdec(substr($data, 10,2))* ($res_ev / 255));
		$data7 = (hexdec(substr($data, 12,2))* ($res_ev / 255));
		$data8 = (hexdec(substr($data, 14,2))* ($res_ev / 255));
		//
		$M1 = (hexdec(substr($data, 16,2)));
		$m1 = $M1 & $mask_m1_5;
		$m2 = ($M1 & $mask_m2_6)>>2;
		$m3 = ($M1 & $mask_m3_7)>>4;
		$m4 = ($M1 & $mask_m4_8)>>6;
		//
		$M2 = (hexdec(substr($data, 18,2)));
		$m5 = $M2 & $mask_m1_5;
		$m6 = ($M2 & $mask_m2_6)>>2;
		$m7 = ($M2 & $mask_m3_7)>>4;
		$m8 = ($M2 & $mask_m4_8)>>6;
		//
		$flags = (hexdec(substr($data, 20,2)));
		//
		$row->data1 = number_format($res_ev * $m1 + $data1, 2, '.', '');
		$row->data2 = number_format($res_ev * $m2 + $data2, 2, '.', '');
		$row->data3 = number_format($res_ev * $m3 + $data3, 2, '.', '');
		$row->data4 = number_format($res_ev * $m4 + $data4, 2, '.', '');
		$row->data5 = number_format($res_ev * $m5 + $data5, 2, '.', '');
		$row->data6 = number_format($res_ev * $m6 + $data6, 2, '.', '');
		$row->data7 = number_format($res_ev * $m7 + $data7, 2, '.', '');
		$row->data8 = number_format($res_ev * $m8 + $data8, 2, '.', '');
		$row->data9 = 0;
		$row->data10 = 0;
		$row->data11 = 0;
		$row->data12 = 0;
		$row->consumoTxe=0;
		$row->total_eve=0;
		if($row->data1>0){$row->consumoTxe+=$row->data1; $row->total_eve++;}
		if($row->data2>0){$row->consumoTxe+=$row->data2; $row->total_eve++;}
		if($row->data3>0){$row->consumoTxe+=$row->data3; $row->total_eve++;}
		if($row->data4>0){$row->consumoTxe+=$row->data4; $row->total_eve++;}
		if($row->data5>0){$row->consumoTxe+=$row->data5; $row->total_eve++;}
		if($row->data6>0){$row->consumoTxe+=$row->data6; $row->total_eve++;}
		if($row->data7>0){$row->consumoTxe+=$row->data7; $row->total_eve++;}
		if($row->data8>0){$row->consumoTxe+=$row->data8; $row->total_eve++;}
		$row->consumoTxe=number_format($row->consumoTxe,2,'.','');
		$row->total_eve=number_format($row->total_eve,2,'.','');
		return $row;
	}

	private function parser7($device,$data,$time,$seqNumber){
		$row=new RegNivel;
		$row->timestamps=false;
		$row->device=$device;
		$row->seqNumber=$seqNumber;
		$row->data=$data;
		$row->timefox = date('Y-m-d H:i:s', $time);
		$row->unix=$time;
    	$row->data_dec = hexdec(substr($data, 0,2));
		return $row;
	}

	private function parser9($device,$data,$time,$seqNumber){
		$mag=substr($data, 0, 4);
		$mag=base_convert($mag,16,10);
		$pal=substr($data, 4, 4);
		$pal=base_convert($pal,16,10);
		$subStrAux=substr($data,8,2);
		$subStrAux=sprintf( "%08b",base_convert($subStrAux,16,10));
		$c1=substr($subStrAux,0,2);
		$c1=base_convert($c1,2,10);
		$c2=substr($subStrAux,2,2);
		$c2=base_convert($c2,2,10);
		$c3=substr($subStrAux,4,2);
		$c3=base_convert($c3,2,10);
		$gate=substr($subStrAux,6,1);
		$subStrAux=substr($data,10,6);
		$subStrAux=sprintf( "%024b",base_convert($subStrAux,16,10));
		$p1=substr($subStrAux,0,6);
		$p1=base_convert($p1,2,10);
		$p2=substr($subStrAux,6,6);
		$p2=base_convert($p2,2,10);
		$p3=substr($subStrAux,12,6);
		$p3=base_convert($p3,2,10);
		$temperature=substr($subStrAux,18,6);
		$temperature=base_convert($temperature,2,10);
		$row=new SytesaData();
		$row->timestamps=false;
		$row->seqNumber=$seqNumber;
		$row->data=$data;
		$row->timefox =date('Y-m-d H:i:s', $time);
		$row->unix=$time;
		$row->device=$device;
		$row->data=$data;
		$row->pal=$pal;
		$row->mag=$mag;
		$row->c1=$c1;
		$row->c2=$c2;
		$row->c3=$c3;
		$row->gate=$gate;
		$row->p1=$p1;
		$row->p2=$p2;
		$row->p3=$p3;
		$row->temperature=$temperature;
		$row->timesys=date("Y-m-d H:i:s");
		return $row;
	}
}