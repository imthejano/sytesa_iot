<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use Illuminate\Support\Facades\Hash;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $toJson=$request->input('toJson',true);
        $device=new Device;
        $device->sigfox_id=$request->input('sigfox_id');
        $device->sigfox_id_hash=Hash::make($request->input('sigfox_id'));
        $device->type=$request->input('type',0);
        $device->save();
        if($toJson)json_encode($device);
        return $device;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$hash){
        $set['status']=$request->input('set_status');
        $set['type']=$request->input('set_type');
        $device=Device::where('sigfox_id_hash',$hash)->first();
        foreach ($set as $key => $setting) {
            $device[$key]=$setting;
        }
        $device->update();
    }

    public function activate(Request $request){
        $hash=$request->input('sigfox_id_hash');
        $toJson=$request->input('toJson',true);
        $device=Device::where('sigfox_id_hash',$hash)->first();
        if($device!=null){
            switch($device->status){
                case 0:
                    $device->status=1;
                    $device->update();
                    $response['message']='Device has been activated';
                    $response['success']=true;
                    break;
                case 1:
                    $response['message']='Device had been already activated';
                    $response['success']=false;
                    break;
                case 2:
                    $response['message']='Device is disabled and can not be activated';
                    $response['success']=false;
                    break;
                default:
                    $response['message']='Unknow status, device can not been activated';
                    $response['success']=false;
                    break;
            }
        }else{
            $response['message']='device was not found';
            $response['success']=false;
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
