<?php

namespace App\Http\Controllers;

use App\Supervisor;
use Illuminate\Http\Request;
use App\Region;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function show($supervisor,Request $request){
        $toJson=$request->input('toJson',true);
        $withPlants=$request->input('with_plants',false);
        $withPlantsRegion=$request->input('with_plants_region',false);
        $withRegions=$request->input('with_regions',false);
        $findBy=$request->input('find_By','id');
        $supervisor=Supervisor::where($findBy,$supervisor);
        $supervisor=$supervisor->first();
        if($withPlants){
            $supervisor->plants=$supervisor->plants()->get();
            if($withPlantsRegion){
                foreach ($supervisor->plants as $key => $plant) {
                    $plant->region=$plant->region()->first();
                }
            }
        }
        if($withRegions){
            $supervisor->regions=$supervisor->regions()->get();
        }
        $response['supervisor']=$supervisor;
        if($toJson)return json_encode($response);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function edit(Supervisor $supervisor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supervisor $supervisor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supervisor $supervisor)
    {
        //
    }

}
