<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataTest extends Model
{
    protected $table='data';
    public $timestamps = false;
}
