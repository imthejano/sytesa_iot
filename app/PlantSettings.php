<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantSettings extends Model
{
    protected $table = 'sytesa_plant_settings';
}
