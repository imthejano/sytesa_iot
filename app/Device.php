<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'sytesa_devices';
    public function plant()
    {
        return $this->belongsTo('App\Plant');
    }
}
