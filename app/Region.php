<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'sytesa_regions';   
    
    
    public function supervisors()
    {
        return $this
        ->belongsToMany('App\Supervisor','sytesa_region_supervisor','region_id','supervisor_id');
    }
}
