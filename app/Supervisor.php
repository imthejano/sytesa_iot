<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{


    protected $table = 'users';

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function regions()
    {
        return $this
        ->belongsToMany('App\Region','sytesa_region_supervisor','supervisor_id','region_id');
    }

    public function plants()
    {
        return $this->hasMany('App\Plant', 'supervisor_id', 'id');
    }
}
