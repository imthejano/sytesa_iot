<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $table = 'sytesa_plants';

    public function device()
    {
        return $this->hasOne('App\Device', 'device_id', 'id');
    }
    public function supervisor()
    {
        return $this->belongsTo('App\Supervisor', 'supervisor_id', 'id');
    }
    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id', 'id');
    }
    
    public function settings()
    {
        return $this->belongsTo('App\PlantSettings', 'settings_id', 'id');
    }

}
