@php
	$plant=App\Plant::where('device_id',$device->id)->first();
	$settings=$plant->settings()->first();
@endphp
<div class="row">
	<div class="col-12 col-lg-2">
		<div class="row my-0">
			<div class="col-12">
				<h6><i class="fas fa-magnet fa-2x"></i> Magnetico</h6>
				<h5>Dispositivo <span class="badge badge-warning text-white">{{$device->sigfox_id}}</span></h5>
				<h5>Ultimo registro: 
					<span id="lastInput">
						<div class="spinner-border text-secondary text-sm" role="status">
							<span class="sr-only">Loading...</span>
						</div>
					</span>
				</h5>
			</div>
		</div>
		<div class="row my-0 mb-2 d-none d-md-block">
			<div class="col-12">
				<form action="">
					<div class="form-group">
						<label for="formControlRange">Maximo</label>
						<input class="form-control" type="number" disabled id="input-maxVal" default="{{$settings->maxMag}}" value="{{$settings->maxMag}}">
					</div>
					<div class="form-group">
						<label for="formControlRange">Minimo</label>
						<input class="form-control" type="number" disabled id="input-minVal" default="{{$settings->minMag}}" value="{{$settings->minMag}}">
					</div>
					<button class="my-2 btn btn-block btn-info" id="btn-edit">Editar</button>
					<button class="my-2 btn btn-block btn-success" hidden id="btn-save">Guardar</button>
					<button class="my-2 btn btn-block btn-danger" hidden id="btn-cancel">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
	<div class="col-12 col-lg-10 py-0">
		<div class="row mt-md-4">
			<div class="ml-3 btn-group" role="group" aria-label="First group">
				<label class="input-group-text rounded-0" id="btnGroupAddon" for="range">
					Filtro
					<span class="add-on"><i class="far fa-calendar-alt" style="margin-left: 6px;"></i></span>
				</label>
				<input class="form-control rounded-0 text-muted"  type="text" name="range" id="range">
				<button type="button" class="d-none d-lg-block btn btn-outline-secondary" onclick="updateSensordataCollected(null,null,range='today')">Hoy</button>
				<button type="button" class="d-none d-lg-block btn btn-outline-secondary" onclick="updateSensordataCollected(null,null,range='yesterday')">Ayer</button>
				<button type="button" class="d-none d-lg-block btn btn-outline-secondary" onclick="updateSensordataCollected(null,null,range='week')">semana</button>
				<button type="button" class="d-none d-lg-block btn btn-outline-secondary" onclick="updateSensordataCollected(null,null,range='month')">mes</button>
				<button type="button" class="d-none d-lg-block btn btn-outline-secondary" onclick="reload();"><i class="fas fa-redo-alt"></i></button>
			</div>
		</div>
		<div class="row mt-2">
			<div class="col-12">
				<div class="card border-1 rounded-0" style="height: 11rem">
					<div class="card-body">
						<div id="chartdDtaCollected">
							<div class="spinner-border mx-auto" role="status">
								<span class="sr-only">Loading...</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-2">
			<div class="col-12 col-lg-6">
				<div class="card border-1 rounded-0">
					<div class="card-body">
						<h5 class="card-title">Historico</h5>
						<div class="table-responsive" style="height: 9rem">
							<table class="table table-light">
								<thead>
									<tr>
										<td>Fecha</td>
										<td>Dato</td>
										<td>Status</td>
									</tr>
								</thead>
								<tbody id="dataTable">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6">
				<div id="chartWeek">
					<div class="spinner-border mx-auto" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var sensor="magnetico";
	var minVal={{$settings->minMag}};
	var maxVal={{$settings->maxMag}};
	$(document).ready(function(){
		var start = moment().startOf('day'),
			end = moment();
		var range = $('#range');
		range.val('Desde ' + start.format('MM/DD/YYYY') + ' hasta ' + end.format('MM/DD/YYYY'));
		range.daterangepicker({
			timePicker: true,
			startDate:start,
			endDate:end,
			maxDate:moment(),
			locale: {
				format: 'M/DD hh:mm A'
			},
			ranges: {
				'Ultimos 30 días': [moment().subtract(29, 'days').startOf('day'), moment()],
				'Ultimos 7 días': [moment().subtract(6, 'days').startOf('day'), moment()],
				'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
				'Hoy': [moment().startOf('day'), moment()],
				'Ultimas 5 horas': [moment().subtract(5, 'hour'), moment()],
				'Ultimas 2 horas': [moment().subtract(2, 'hour'), moment()],
				'Ultima hora': [moment().subtract(1, 'hour'), moment()],
			},
		}, function (start, end) {
			updateSensordataCollected(start,end);
		});
		updateSensordataCollected(start,end);
		drawChartWeek();
	});


	function updateSensordataCollected(from,to,range=null){
		$('#dataTable').html(''
			+'<div class="spinner-border text-secondary text-sm" role="status">'
			+'	<span class="sr-only">Loading...</span>'
			+'</div>'
		);
		$('#chartdDtaCollected').html('<canvas id="cChartDataCollected"></canvas>');
		if(range!=null){
			switch(range){
				case 'today':
					from=moment().startOf('day');
					to=moment();
					$('#range').val(moment(from).format('M/DD HH:mm')+' - '+moment(to).format('M/DD HH:mm'));
					break;
				case 'yesterday':
					from=moment().subtract(1, 'days').startOf('day');
					to=moment().subtract(1, 'days').endOf('day');
					$('#range').val(moment(from).format('M/DD HH:mm')+' - '+moment(to).format('M/DD HH:mm'));
					break;
				case 'month':
					from=moment().startOf('month');
					to=moment().endOf('month');
					$('#range').val(moment(from).format('M/DD HH:mm')+' - '+moment(to).format('M/DD HH:mm'));
					break;
				case 'week':
					from=moment().startOf('week');
					to=moment().endOf('week');
					$('#range').val(moment(from).format('M/DD HH:mm')+' - '+moment(to).format('M/DD HH:mm'));
					break;
			}
		}
		$.ajax({
			type:'GET',
			url:'/data/get',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'from':from.format('YYYY-MM-DD HH:mm:ss'),
				'to':to.format('YYYY-MM-DD HH:mm:ss'),
				'toJson':true,
				'device':'{{$device->sigfox_id}}',
			},
			success:function(response){
				//collecting data
				//console.log(response);
				data=JSON.parse(response);
				dataCollected=[];
				dateTime=[];
				$('#dataTable').html('');
				$.each(data['sytesa_data'],function(i,row){
					dataCollected[i]=row['mag'];
					dateTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
					dc=parseFloat(dataCollected[i]);
					maxVal=parseFloat(maxVal);
					minVal=parseFloat(minVal);
					status=(dc>=minVal && dc<=maxVal)?'<i class="text-success fas fa-check"></i>':'<i class="text-danger fas fa-exclamation-triangle"></i>';
					$('#dataTable').append(''
					+'<tr>'
					+'	<td>'+dateTime[i]+'</td>'
					+'	<td>'+dataCollected[i]+'</td>'
					+'	<td>'+status+'</td>'
					+'</tr>'
				);
				});
				$('#lastInput').html(dataCollected[0])
				//updating chart
				canv1=$('#cChartDataCollected');
				var canv1 = cChartDataCollected.getContext("2d");
				var gradientStroke1 = canv1.createLinearGradient(0, 230, 0, 50);
				gradientStroke1.addColorStop(1, selectColor('c1',.4));
				gradientStroke1.addColorStop(.5, selectColor('c1',.5));
				gradientStroke1.addColorStop(.2, selectColor('c1',.9));
				gradientStroke1.addColorStop(0, selectColor('c1',1));
				drawChart($('#cChartDataCollected'), dateTime.reverse(), dataCollected.reverse(), 'Lts', gradientStroke1, type = 'line');
			},
			error:function(xhr){
				console.log(xhr.responseText);
				
			}
		});
	}
	function drawChartWeek(){
		$('#chartWeek').html('<canvas id="cChartWeek"></canvas>');
		$.ajax({
			type:'GET',
			url:'/data/get',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'from':moment().startOf('week').format('YYYY-MM-DD HH:mm:ss'),
				'to':moment().format('YYYY-MM-DD HH:mm:ss'),
				'toJson':true,
				'device':'{{$device->sigfox_id}}',
			},
			success:function(response){
				data=JSON.parse(response);
				var weekVals={0:0,1:0,2:0,3:0,4:0,5:0,6:0};
				$.each(data.sytesa_data,function(i,d){
					weekVals[parseInt(d.dayofweek)-1]+=parseInt(d.mag);
				});
				var ctx = document.getElementById('cChartWeek').getContext('2d');
				var myChart = new Chart(ctx, {
				    type: 'bar',
				    data: {
				        labels: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
				        datasets: [{
							fill: true,
							borderDash: [],
							borderDashOffset: 5.0,
				            data: [weekVals[0],weekVals[1],weekVals[2],weekVals[3],weekVals[4],weekVals[5],weekVals[6]],
				            backgroundColor: ['rgba(255, 99, 132, 0.7)','rgba(54, 162, 235, 0.7)','rgba(255, 206, 86, 0.7)','rgba(75, 192, 192, 0.7)','rgba(153, 102, 255, 0.7)','rgba(255, 159, 64, 0.7)'],
				            borderWidth: 0
				        }]
				    },
				    options: {
						legend: {display: false},
				        scales: {
				            yAxes: [{display:false}]
				        },
						tooltips: {
							backgroundColor:'rgba(250,250,250,0)',
							titleFontColor:'#11111',
							bodyFontColor:'#11111',
							displayColors:false,
							xPadding:30,
        				}
				    }
				});
			}
		});
	}

	$('#btn-edit').click(function (e) { 
		e.preventDefault();
		$('#input-maxVal').removeAttr('disabled');
		$('#input-minVal').removeAttr('disabled');
		$('#btn-save').removeAttr('hidden');
		$('#btn-cancel').removeAttr('hidden');
		$('#btn-edit').attr('hidden',true);
	});
	$('#btn-cancel').click(function (e) { 
		e.preventDefault();
		$('#input-maxVal').attr('disabled',true);
		$('#input-minVal').attr('disabled',true);
		$('#btn-save').attr('hidden',true);
		$('#btn-cancel').attr('hidden',true);
		$('#btn-edit').removeAttr('hidden',true);
		$('#input-maxVal').val($('#input-maxVal').attr('default'));
		$('#input-minVal').val($('#input-minVal').attr('default'));
	});
	$('#btn-save').click(function (e) { 
		e.preventDefault();
		saveSettings();
		$('#input-maxVal').attr('disabled',true);
		$('#input-minVal').attr('disabled',true);
		$('#btn-save').attr('hidden',true);
		$('#btn-cancel').attr('hidden',true);
		$('#btn-edit').removeAttr('hidden',true);
	});

	function saveSettings(){
		$.ajax({
			type:'PUT',
			url:'/plant/settings/update',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'plant':'{{$plant->name}}',
				'findBy':'name',
				'set_maxMag':$('#input-maxVal').val(),
				'set_minMag':$('#input-minVal').val(),
			},
			success:function(response){
				console.log(response);
				var start = moment().startOf('day'),
				end = moment();
				$('#input-maxVal').attr('default',$('#input-maxVal').val());
				$('#input-minVal').attr('default',$('#input-minVal').val());
				maxVal=$('#input-maxVal').val();
				minVal=$('#input-minVal').val();
				updateSensordataCollected(start,end);
			},
			error:function(xhr){
				console.log(xhr.responseText);
				alert(xhr.responseText);
				
			}
		});
	}

	function drawChart(canvas, labels, dataX, label, color,type, displayAxes=true,displayLegend=false) {
		var ctx = canvas;
		var myChart = new Chart(
			ctx, {type: type,responsive: true,legend: {display: false},
			data: {
				labels: labels,
				datasets: [{
					label: label,
					fill: true,
					backgroundColor: color,
					hoverBackgroundColor: color,
					//borderColor: '#1f8ef1',
					borderWidth: 1,
					borderDash: [],
					borderDashOffset: 0.0,
					data: dataX
				}]
			},
			options: {
				color: function(context) {
				var index = context.dataIndex;
				var value = context.dataset.data[index];
				return value < minVal ? 'red' :  // draw negative values in red
					index % 2 ? 'blue' :    // else, alternate values in blue and green
					'green';
				},
				maintainAspectRatio: false,
				legend: {display: false},
				tooltips: {
					backgroundColor: 'rgba(0,0,0,0)',
					titleFontColor: '#333',
					bodyFontColor: '#1111',
				},
				responsive: true,
				scales: {
					yAxes: [{display:false}],
					xAxes: [
						{barThickness:5,  barPercentage: 1.00,display:false,gridLines: {drawBorder: false,color: 'rgba(29,140,248,0.1)',zeroLineColor: "transparent",},
							ticks: {padding: 20,fontColor: "#9a9a9a"},
							scaleLabel: {display: false,labelString: label,fontSize: 20}
					}]
				}
			},
		});
	}
	function reload(){
		$('.ajs-content').html(''
			+'<div class="spinner-border text-secondary text-sm" role="status">'
			+'	<span class="sr-only">Loading...</span>'
			+'</div>'
		);
		$('.ajs-content').load('/devices/{{$device->sigfox_id}}/panel/mag');
	}
</script>