@php
	$plant=App\Plant::where('device_id',$device->id)->first();
	$settings=$plant->settings()->first();
@endphp
<div class="row">
	<div class="col-2">
		<div class="card border-0 rounded-0" style="height: 12rem">
			<div class="card-body p-1">
				<h6 id="header"></h6>
				<h5 class="card-title">Dispositivo <span class="badge badge-warning text-white">{{$device->sigfox_id}}</span></h5>
				<h5 class="card-title">Sensor <span class="badge badge-warning text-white">{{$sensor}}</span></h5>
				<h5>valor actual: 
					<span id="lastInput">
						<div class="spinner-border text-secondary text-sm" role="status">
							<span class="sr-only">Loading...</span>
						</div>
					</span>
				</h5>
				<form action="">
					<div class="form-group">
						<label for="formControlRange">Maximo</label>
						<input class="form-control" type="text" disabled id="maxVal">
					</div>
					<div class="form-group">
						<label for="formControlRange">Minimo</label>
						<input class="form-control" type="text" disabled id="minVal">
					</div>
					<button class="btn btn-block btn-info">Editar</button>
				</form>
			</div>
		</div>
	</div>
	<div class="col-10 py-0">
		<div class="row mt-2">
			<div class="col-12">
				<div class="card border-1 rounded-0" style="height: 11rem">
					<div class="card-body">
						<div id="chartdDtaCollected">
							<div class="spinner-border mx-auto" role="status">
								<span class="sr-only">Loading...</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-2">
			<div class="col-6">
				<div class="card border-1 rounded-0">
					<div class="card-body">
						<h5 class="card-title">Historico</h5>
						<div class="table-responsive" style="height: 9rem">
							<table class="table table-light">
								<thead>
									<tr>
										<td>Fecha</td>
										<td>Dato</td>
									</tr>
								</thead>
								<tbody id="dataTable">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-6">
				<img src="{{asset('img/nivel.gif')}}" class="ml-5" alt="">
			</div>
		</div>
	</div>
</div>
<script>
	var sensorArr={
		'paletas':'pal',
		'magnetico':'mag',
		'carcamo1':'c1',
		'carcamo2':'c2',
		'carcamo3':'c3',
		'puerta':'gate',
		'presion1':'p1',
		'presion2':'p2',
		'presion3':'p3',
		'temperatura':'temperature',
	};
	var settingsArr={
		'maxpal':{{$settings->maxPal}},
		'minpal':{{$settings->minPal}},
		'maxmag':{{$settings->maxMag}},
		'minmag':{{$settings->minMag}},
		'maxtemperature':{{$settings->maxTemperature}},
		'mintemperature':{{$settings->minTemperature}},
		'maxgate':{{$settings->maxGate}},
		'mingate':{{$settings->minGate}},
		'maxc1':{{$settings->maxC1}},
		'minc1':{{$settings->minC1}},
		'maxc2':{{$settings->maxC2}},
		'minc2':{{$settings->minC2}},
		'maxc3':{{$settings->maxC3}},
		'minc3':{{$settings->minC3}},
		'maxp1':{{$settings->maxP1}},
		'minp1':{{$settings->minP1}},
		'maxp2':{{$settings->maxP2}},
		'minp2':{{$settings->minP2}},
		'maxp3':{{$settings->maxP3}},
		'minp3':{{$settings->minP3}},
	};
	var sensor="{{$sensor}}"
	$(document).ready(function(){
		from=moment().startOf('hour');
		to=moment();
		updateSensordataCollected(from,to);
		$('#maxVal').val(settingsArr['max'+sensorArr[sensor]]);
		$('#minVal').val(settingsArr['min'+sensorArr[sensor]]);
		switch (sensorArr[sensor]) {
			case 'mag': $('#header').html('<i class="fas fa-tint fa-2x"></i>'); break;
			case 'pal': $('#header').html('<i class="fas fa-tint fa-2x"></i>'); break;
			case 'temperature': $('#header').html('<i class="fas fa-thermometer-quarter fa-2x"></i>'); break;
			case 'c1': $('#header').html('<i class="fas fa-tint fa-2x"></i>'); break;
			case 'c2': $('#header').html('<i class="fas fa-tint fa-2x"></i>'); break;
			case 'c3': $('#header').html('<i class="fas fa-tint fa-2x"></i>'); break;
			case 'p1': $('#header').html('<i class="fas fa-tachometer-alt fa-2x"></i>'); break;
			case 'p2': $('#header').html('<i class="fas fa-tachometer-alt fa-2x"></i>'); break;
			case 'p3': $('#header').html('<i class="fas fa-tachometer-alt fa-2x"></i>'); break;
		
			default:
				break;
		}
	});
	function updateSensordataCollected(from,to){
		$('#chartdDtaCollected').html('<canvas id="cChartDataCollected"></canvas>');
		$.ajax({
			type:'GET',
			url:'/api/data/get',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'from':from.format('YYYY-MM-DD HH:mm:ss'),
				'to':to.format('YYYY-MM-DD HH:mm:ss'),
				'toJson':true,
			},
			success:function(response){
				//collecting data
				data=JSON.parse(response);
				dataCollected=[];
				dateTime=[];
				$.each(data['sytesa_data'],function(i,row){
					dataCollected[i]=row[sensorArr[sensor]];
					dateTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
					$('#dataTable').append('<tr><td>'+dateTime[i]+'</td><td>'+dataCollected[i]+'</td><tr>');
				});
				$('#lastInput').html(dataCollected[0])
				//updating chart
				canv1=$('#cChartDataCollected');
				var canv1 = cChartDataCollected.getContext("2d");
				var gradientStroke1 = canv1.createLinearGradient(0, 230, 0, 50);
				gradientStroke1.addColorStop(1, selectColor('c1',.4));
				gradientStroke1.addColorStop(.5, selectColor('c1',.5));
				gradientStroke1.addColorStop(.2, selectColor('c1',.9));
				gradientStroke1.addColorStop(0, selectColor('c1',1));
				drawChart($('#cChartDataCollected'), dateTime.reverse(), dataCollected.reverse(), 'Lts', gradientStroke1, type = 'line');
			},
			error:function(xhr){
				console.log(xhr.responseText);
				alert(xhr.responseText);
				
			}
		});
	}

	function drawChart(canvas, labels, dataX, label, color,type, displayAxes=true,displayLegend=false) {
		var ctx = canvas;
		var myChart = new Chart(
			ctx, {type: type,responsive: true,legend: {display: false},
			data: {
				labels: labels,
				datasets: [{
					label: label,
					fill: true,
					backgroundColor: color,
					hoverBackgroundColor: color,
					//borderColor: '#1f8ef1',
					borderWidth: 1,
					borderDash: [],
					borderDashOffset: 0.0,
					data: dataX
				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {display: false},
				tooltips: {
					backgroundColor: '#f5f5f5',
					titleFontColor: '#333',
					bodyFontColor: '#666',
					bodySpacing: 4,
					xPadding: 12,
					mode: "nearest",
					intersect: 0,
					position: "nearest"
				},
				responsive: true,
				scales: {
					yAxes: [
						{barPercentage: 1,display:displayAxes,gridLines: {drawBorder: false,color: 'rgba(29,140,248,0.1)',zeroLineColor: "transparent",},
							ticks: {suggestedMin: 0.1,suggestedMax: 0.15,padding: 20,fontColor: "#9a9a9a"},
							scaleLabel: {display: false,labelString: label,fontSize: 20}
					}],
					xAxes: [
						{barThickness:5,  barPercentage: 1.00,display:displayAxes,gridLines: {drawBorder: false,color: 'rgba(29,140,248,0.1)',zeroLineColor: "transparent",},
							ticks: {padding: 20,fontColor: "#9a9a9a"},
							scaleLabel: {display: false,labelString: label,fontSize: 20}
					}]
				}
			},
		});
	}
</script>