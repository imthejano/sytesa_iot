@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-lg-6">
			<center>
				<img src="{{asset('img/057.jpg')}}" class="mx-automl-6" style="height: 25rem;" alt="">
			</center>
		</div>
		<div class="col-lg-6 " style="padding:15px 50px 0 0;margin: auto">
				<strong style="font-size: 25px">SYTESA</strong> es una empresa 100% mexicana que contribuye al cuidado del medio ambiente poniendo en manos de nuestros clientes la mejor tecnología, para lograr la sustentabilidad económica, ecológica y operativa del ciclo del agua. Nos enfocamos en brindar la mejor alternativa para lograr el reúso de agua.
		</div>
	</div>
	<div class="row mt-5 px-5">
		<div class="col-4">
			<center>
				<img src="https://sytesa.com.mx/wp-content/uploads/2016/01/01_drop.svg" class="vc_single_image-img attachment-full" style="height: 5rem"alt="">
				<br><h6 class="mt-4"AHORRAMOS<br>AGUA POTABLE</h6>
			</center>
			</div>
		<div class="col-4">
			<center>
				<img src="https://sytesa.com.mx/wp-content/uploads/2016/01/02_pipe.svg" class="vc_single_image-img attachment-full" style="height: 5rem"alt="">
				<br><h6 class="mt-4">DISMINUIMOS LA DESCARGA A DRENAJE</h6>
			</center>
			</div>
		<div class="col-4">
			<center>
				<img src="https://sytesa.com.mx/wp-content/uploads/2016/01/03_nom.svg" class="vc_single_image-img attachment-full" style="height: 5rem"alt="">
				<br><h6 class="mt-4">CUMPLIMOS NORMAS MEXICANAS</h6>
			</center>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{asset('lib\chartjs.min.js')}}"></script>
	<script>
		var oilCanvas = document.getElementById("chart");
		var oilData = {
			labels: [
				"Region 1",
				"Region 2",
				"Region 3",
				"Region 4",
				"Region 5",
			],
			datasets: [
				{
					data: [133.3, 86.2, 52.2, 51.2, 50.2],
					backgroundColor: [
						"#7B1FA2",
						"#448AFF",
						"#3F51B5",
						"#009688",
						"#00BCD4"
					]
				}]
		};
		var pieChart = new Chart(oilCanvas, {
			type: 'pie',
			data: oilData
		});
	</script>
@endsection