<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'name='viewport' />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" type="image/png" href=" {{url('favicon.ico')}} ">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{url('lib/alertify/css/alertify.min.css')}}" rel="stylesheet">
	{{-- <link rel="stylesheet" href="{{url('lib/datatables/Buttons-1.5.6/css/buttons.bootstrap.min.css')}}"> --}}
	{{-- <link rel="stylesheet" href="{{url('lib/datatables/datatables.min.css')}}"> --}}
	@yield('styleSheets')
	<title>@yield('title')</title>
	<style>
		.bg1{
			background: rgba(0,58,112,1);
			background: -moz-linear-gradient(-45deg, rgba(0,58,112,1) 27%, rgba(150,222,172,1) 91%, rgba(171,245,180,1) 100%);
			background: -webkit-gradient(left top, right bottom, color-stop(27%, rgba(0,58,112,1)), color-stop(91%, rgba(150,222,172,1)), color-stop(100%, rgba(171,245,180,1)));
			background: -webkit-linear-gradient(-45deg, rgba(0,58,112,1) 27%, rgba(150,222,172,1) 91%, rgba(171,245,180,1) 100%);
			background: -o-linear-gradient(-45deg, rgba(0,58,112,1) 27%, rgba(150,222,172,1) 91%, rgba(171,245,180,1) 100%);
			background: -ms-linear-gradient(-45deg, rgba(0,58,112,1) 27%, rgba(150,222,172,1) 91%, rgba(171,245,180,1) 100%);
			background: linear-gradient(135deg, rgba(0,58,112,1) 27%, rgba(150,222,172,1) 91%, rgba(171,245,180,1) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#003a70', endColorstr='#abf5b4', GradientType=1 );
		}
		.bgC1{
			background: #4078D5;
		}
		.bgC2{
			background: #40D598;
		}
		.bgC3{
			background: #40C8D5;
		}
		.bgC4{
			background: #D57440;
		}
		.bgC5{
			background: #40D598;
		}
		.bgC6{
			background: #D5405B;
		}
		.bgCLight{
			background: #ECF1F9;
		}

		.map-responsive{
			position: relative;
			padding-bottom: 75%; // This is the aspect ratio
			height: 0;
			overflow: hidden;
		}
		.map-responsive iframe{
			position: absolute;
			top: 0;
			left: 0;
			width: 100% !important;
			height: 100% !important;
}
	</style>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark mb-4 py-0 py-md-2 bg1">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand my-0">
			<img class="my-0" src="{{asset('img/logo_sytesa.png')}}" alt="" style="height: 3rem;">
		</a>
		<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
			<ul class="navbar-nav ml-auto">
				@yield('navContent')
				<li class="nav-item">
					@if (Auth::check())
					<div class="dropdown mr-5">
						<button type="button" class="rounded-pill btn btn-outline-light dropdown-toggle" id="btn-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-user"></i><span class="ml-2">{{auth()->user()->userName}}</span>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
							<a class="btn btn-link text-black mx-auto dropdown-item" href="" class="dropdown-item">Cuenta</a>
							<a class="btn btn-link text-black mx-auto dropdown-item" href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
								Logout
							</a>
							<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</div>
					</div>
					@else
					<button type="button" class="btn btn-outline-light mx-auto" id="btn-login">Login</button>
					@endif
				</li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid px-4">
		@yield('content')
	</div>
</body>
@if (!Auth::check())
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLoginLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content rounded-0 border-0 mx-auto" style="width: 20rem;">
			<div class="modal-header">
				<h5 class="modal-title" id="modalLoginLabel">Login</h5>
			</div>
			<div class="modal-body p-4">
				<form action="api/regions/web/access" method="POST" id="login-form" onsubmit="login(evt)">
					<div class="form-row">
						<div class="col-12">
							<ul id="login-logs">

							</ul>
						</div>
					</div>
					<div class="form-row my-2">
						<div class="col-12">
							<center>
								<img src="{{asset('img/user.png')}}" alt="" style="height: 10rem;">
							</center>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="my-input">Usuario</label>
								<input id="input-user" class="form-control" type="text" name="input-user" placeholder="Usuario o correo">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="my-input">Password</label>
								<input id="input-password" class="form-control" type="password" name="input-password" placeholder="password">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<div class="custom-control custom-checkbox">
								<input class="custom-control-input" type="checkbox" id="input-remember" >
								<label class="custom-control-label" for="input-remember">
									Recordarme
								</label>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				  <button type="submit" class="btn btn-info" id="login-submit">Acceder</button>
			</div>
		</div>
	</div>
</div>
@endif
	<script src="{{url('js/jquery.min.js') }}"></script>
	<script src='{{url('js/bootstrap.min.js')}}'></script>
	{{-- <script src="{{url('lib/datatables/Buttons-1.5.6/js/buttons.bootstrap.min.js')}}"></script> --}}
	{{-- <script src="{{url('lib/datatables/datatables.min.js')}}"></script> --}}
	<script src="{{url('lib/alertify/alertify.min.js')}}"></script>
	<script>
		var _region='{{isset($region)?$region->id:''}}';
		var _supervisor='{{isset($supervisor)?$supervisor->id:''}}';
		var _plant='{{isset($plant)?$plant->id:''}}';
		var _regionName='{{isset($region)?$region->name:''}}';
		var _supervisorName='{{isset($supervisor)?$supervisor->name:''}}';
		var _plantName='{{isset($plant)?$plant->name:''}}';
		@if(Auth::check())
			@if(auth()->user()->roles()->first()->id==7)
				$(document).ready(function(){
					loadRegions();
					if(_region!='')loadSupervisors(_region);
					if(_supervisor!='')loadPlants(_supervisor);
				});
				regionsArr={};
				supervisorsArr={};
				plantsArr={};
				function loadRegions(){
					$('#plantsSelect').attr('disabled','true');
					$('#supervisorsSelect').attr('disabled','true');
					$('#regionsSelect').attr('disabled','true');
					$('#regionsSelect').html('<option value="0">Selecciona región</option>');
					$.ajax({
						type:'GET',
						url:'/regions/get',
						headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
						success: function(response){
							data=JSON.parse(response);
							$.each(data['regions'],function(i,region){
								regionsArr[region.id]=region;
								selected=_region==region.id?'selected':'';
								$('#regionsSelect').append('<option value="'+region.id+'" '+selected+'>'+region.name+'</option>');
							});
							$('#regionsSelect').removeAttr('disabled');
						}
					});
				}
				
				function loadSupervisors(regionId){
					$('#plantsSelect').attr('disabled','true');
					$('#supervisorsSelect').attr('disabled','true');
					$('#supervisorsSelect').html('<option value="0">Selecciona supervisor</option>');
					$.ajax({
						type:'GET',
						url:'/region/'+regionId,
						data:{
							'with_supervisors':true,
							'findBy':'id',
						},
						headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
						success: function(response){
							//console.log(response);
							data=JSON.parse(response);
							region=data.region;
							$.each(region.supervisors,function(i,supervisor){
								selected=_supervisor==supervisor.id?'selected':'';
								supervisorsArr[supervisor.id]=supervisor;
								$('#supervisorsSelect').append('<option value="'+supervisor.id+'" '+selected+'>'+supervisor.userName+'</option>');
							});
							$('#supervisorsSelect').removeAttr('disabled');
						}
					});
				}
				function loadPlants(supervisorId){
					$('#plantsSelect').attr('disabled','true');
					$('#plantsSelect').html('<option value="0">Selecciona planta</option>');
					$.ajax({
						type:'GET',
						url:'/supervisor/'+supervisorId,
						data:{
							'with_plants':true,
							'findBy':'id',
						},
						headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
						success: function(response){
							//onsole.log(response);
							data=JSON.parse(response);
							supervisor=data.supervisor;
							$.each(supervisor.plants,function(i,plant){
								plantsArr[plant.id]=plant;
								$('#plantsSelect').append('<option value="'+plant.id+'">'+plant.name+'</option>');
							});
							$('#plantsSelect').removeAttr('disabled');
						}
					});
				}

			@elseif(auth()->user()->roles()->first()->id==8)
				plantsArr={};
				function loadPlants(supervisorId){
					$('#plantsSelect').attr('disabled','true');
					$('#plantsSelect').html('<option value="0">Selecciona planta</option>');
					$.ajax({
						type:'GET',
						url:'/supervisor/'+supervisorId,
						data:{
							'with_plants':true,
							'findBy':'id',
						},
						headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
						success: function(response){
							//console.log(response);
							data=JSON.parse(response);
							supervisor=data.supervisor;
							$.each(supervisor.plants,function(i,plant){
								plantsArr[plant.id]=plant;
								$('#regionsSelect').append('<option value="'+_region+'" selected>'+_regionName+'</option>');
								$('#supervisorsSelect').append('<option value="'+_supervisor+'" selected>'+_supervisorName+'</option>');
								$('#plantsSelect').append('<option value="'+plant.id+'">'+plant.name+'</option>');
							});
							$('#plantsSelect').removeAttr('disabled');
						}
					});
				}
				$(document).ready(function(){
					if(_supervisor!='')loadPlants(_supervisor);
				})
			@endif
		@else
		function login(evt=null){
			evt.preventDefault();
			$('#login-logs').html('<li class="text-secondary">Autenticando...</li>');
			$.ajax({
				type:'POST',
				url:'/web/access',
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				data:{
					'user':$('#input-user').val(),
					'password':$('#input-password').val(),
					'remember':$('#input-remember').prop('checked'),
				},
				success: function(response){
					//console.log(response);
					
					$('#login-logs').html('');
					data=JSON.parse(response);
					if(data.success){
						$('#login-logs').append('<li class="text-secondary">'+data.message+'</li>');
						switch(data['role']){
							case 'superAdmin':
								location.reload();
								break;
							case 'admin':
								location.href="/main";
								break;
							case 'supervisor':
								location.href="/main/region/"+data['region']['name']+"/supervisor/"+data['userName'];
								break;
						}
					}else{
						$.each(data['errors'],function(i,error){
							$('#login-logs').append('<li class="text-danger">'+error+'</li>');
						})
					}
				},
				error: function(xhr){
					$('#login-logs').html(xhr.responseText)
				}
			});
		}
		$(document).ready(function(){
			$('#btn-login').click(function(){
				$('#modalLogin').modal('show');
			});
			$('#login-submit').click(function(evt){
				evt.preventDefault();
				login(evt);
			});
		});
		@endif

		function showAlert(message,settings={'closable':true,'closableByDimmer':true}){
			return alertify.alert(message)
			.set('basic', true)
			.set('modal',true)
			.set('maximizable',true)
			.set(settings)
			;
		}

		function selectColor(color,opacity=1){
			switch(color){
				case 'c1':return 'rgb(0, 214, 180, '+opacity+')';
				case 'c2':return 'rgb(0, 99, 214, '+opacity+')';
				case 'c3':return 'rgb(121, 170, 210, '+opacity+')';
				case 'c4':return 'rgb(255, 154, 2, '+opacity+')';
				case 'c5':return 'rgb(170, 72, 215, '+opacity+')';
				case 'c6':return 'rgb(240, 90, 56, '+opacity+')';
			}
		}

	</script>
	@yield('scripts')

</html>