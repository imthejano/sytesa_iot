@extends('layouts.app')
@php
	$regions=App\Region::all();
@endphp

@section('navContent')
	<li class="nav-item mr-5">
		<form class="form-inline">
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="regionsSelect">
				<option value="0">cargando regiones</option>
			</select>
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="supervisorsSelect">
				<option value="0">Selecciona supervisor</option>
			</select>
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="plantsSelect">
				<option value="0">Selecciona planta</option>
			</select>
		</form>
	</li>
@endsection


@section('content')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent">
		<li class="breadcrumb-item active" aria-current="page">main</li>
	</ol>
</nav>

<div class="row">
	<div class="col-lg-8">
		<div class="row" id="dataCards">
		
		</div>
	</div>
	<div class="col-lg-4">
		<div class="card rounded-0 border-0 shadow">
			<div class="card-body">
				<h5 class="card-title">Info</h5>
				<canvas id="chart"></canvas>
				<p class="mt-3 text-center text-muted">
					Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minima consequuntur officia animi vero temporibus autem consequatur doloribus, voluptatum voluptate error obcaecati accusamus libero ipsa! Ea voluptatibus distinctio ab architecto non?
				</p>
			</div>
		</div>
	</div>
</div>

<div class="row my-3">
	<div class="col-12">
		<div class="card rounded-0 border-0 shadow">
			<div class="card-body">
				<h5 class="card-title">Mapa de regiones</h5>
				<img class="mt-n3" src="{{asset('img/elmapa-mx.png')}}" alt="">
				{{-- <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d7571191.360786669!2d-104.1210977!3d22.0927513!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2smx!4v1562706569163!5m2!1ses!2smx" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		
	</div>
</div>

<div class="row">
	<div class="col-8" id="cards">

	</div>
	<div class="col-4">

	</div>
</div>

@endsection

@section('scripts')
<script src="{{asset('lib\chartjs.min.js')}}"></script>
<script>
	nCols=2
	$(document).ready(function(){
		$('#regionsSelect').change(function(){
			if($('#regionsSelect').val()!=0){
				location.href="/main/region/"+regionsArr[$('#regionsSelect').val()].name;
			}
		});
		$('#dataCards').html(''
		// +'<div class="row">'
		+'	<div class="col-12 text-center align-middle">'
		+'		<p class="text-center lead mx-auto my-0"><div	style="width: 3rem; height: 3rem;" class="spinner-grow text-secondary my-5 align-middle" role="status"></div></p>'
		+'		<p class="text-center lead mx-auto my-0">Cargando</p>'
		+'	</div>'
		// +'</div>'
		);
		$.ajax({
			type:'GET',
			url:'/regions/get',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'with_supervisors':true,
				'with_plants':true,
			},
			success: function(response){
				$('#dataCards').html('');
				console.log(response);
				
				data=JSON.parse(response);
				row=$('#dataCards');
				$.each(data['regions'],function(i,region){
					col=$('<div class="col-lg-'+12/nCols+'">');
					card=$('<div class="card rounded-0 border-0 shadow my-3">').html(''
						+'	<div class="card-body">'
						+'		<h5 class="card-title">'+region.name+'</h5>'
						+'		<ul class="list-group list-group-flush">'
						+'			<li class="list-group-item list-group-item list-group-item-action"><i class="fas fa-user mr-2"></i>Supervisores: <span class="badge badge-primary badge-pill">'+region['supervisors'].length+'</span></li>'
						+'			<li class="list-group-item list-group-item list-group-item-action"><i class="fas fa-tint mr-2"></i>Plantas: <span class="badge badge-primary badge-pill">'+region['plants'].length+'</span></li>'
						+'			<li class="list-group-item list-group-item list-group-item-action">'
						+'				<div class="accordion" id="acordion-region'+region.id+'">'
						+'					<button class="btn btn-link text-secondary" type="button" data-toggle="collapse" data-target="#collapse-region-'+region.id+'" aria-expanded="true" aria-controls="collapse-region-'+region.id+'">'
						+'						Mas info...'
						+'					</button>'
						+'					<div id="collapse-region-'+region.id+'" class="collapse" aria-labelledby="headingOne" data-parent="#acordion-region'+region.id+'">'
						+						region.info
						+'					</div>'
						+'				</div>'
						+'			</li>'
						+'		</ul>'
						+'		<a href="/main/region/'+region.name+'" class="btn btn-outline-dark btn-block rounded-pill mt-3 mb-2">Ir a región</a>'
						+'	</div>'

					);
					col.append(card);
					row.append(col);
				});
			}
		});
		
		var oilCanvas = document.getElementById("chart");
		var oilData = {
			labels: [
				"Region 1",
				"Region 2",
				"Region 3",
				"Region 4",
				"Region 5",
			],
			datasets: [
				{
					data: [133.3, 86.2, 52.2, 51.2, 50.2],
					backgroundColor: [
						"#7B1FA2",
						"#448AFF",
						"#3F51B5",
						"#009688",
						"#00BCD4"
					]
				}]
		};
		var pieChart = new Chart(oilCanvas, {
			type: 'pie',
			data: oilData
		});
	});
</script>
@endsection