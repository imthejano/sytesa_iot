@php
	$plant=App\Plant::where('name',$plant)->first();
	if($plant!=null){
		$device=App\Device::find($plant->device_id);
		$region=$plant->region()->first();
		$supervisor=$plant->supervisor()->first();
	}
@endphp
@extends('layouts.app')
@section('content')
@section('styleSheets')
	<link rel="stylesheet" href="{{url('lib/daterangepicker/daterangepicker.css')}}">
@endsection
	<div class="row">
		<div class="col-md-6">
			<div class="row px-3">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb bg-transparent">
						<li class="breadcrumb-item"><a href="/main">Main</a></li>
						<li class="breadcrumb-item"><a href="/main/region/{{$region->name}}">{{$region->name}}</a></li>
						<li class="breadcrumb-item"><a href="/main/region/{{$region->name}}/supervisor/{{$supervisor->userName}}">{{$supervisor->userName}}</a></li>
						<li class="breadcrumb-item active" aria-current="page">{{$plant->name}}</li>
					</ol>
				</nav>
			</div>
			<div class="row my-2">
				<div class="col-12">
					<div class="card rounded-0 border-0  shadow " style="height: 100%;">
						<div class="card-body">
							<div class="row">
								<div class="col-7">
									<h5 class="card-title">Información de planta</h5>
									<ul class="list-group list-group-flush">
										<li class="list-group-item list-group-item list-group-item-action">Nombre: {{$plant->name}}</li>
										<li class="list-group-item list-group-item list-group-item-action">Dirección: {{$plant->address}}</li>
										<li class="list-group-item list-group-item list-group-item-action">Region: {{$region->name}}</li>
										<li class="list-group-item list-group-item list-group-item-action">Supervisor: {{$supervisor->name}}</li>
									</ul>
								</div>
								<div class="col-4 offset-1">
									<h6>Otra info......</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row my-3">
				<div class="col-12">
					<div class="card rounded-0 border-0 shadow">
						<div class="card-body">
							<h4 class="display">Ubicación</h4>
							<div class="map-responsive">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d{{$plant->longitude}}!2d{{$plant->latitude}}!3d19.465810744663013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d202552e587387%3A0xe9a8d1997a7478d8!2sSoluciones+Y+Tratamiento+Ecol%C3%B3gico%2C+SAPI+De+CV!5e0!3m2!1ses!2smx!4v1563229782838!5m2!1ses!2smx"  frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row my-3">
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style=" height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Magnetico</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard1"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-magnet fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('mag')">Ver detalle</button>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style=" height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Paletas</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard2"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tint fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('pal')">Ver detalle</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row my-3">
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Temperatura</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard3"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-thermometer-quarter fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('temp')">Ver detalle</button>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Puerta</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard4"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-door-open fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('gate')">Ver detalle</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row my-3">
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Cárcamo 1</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard5"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tint fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('c1')">Ver detalle</button>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Cárcamo 2</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard6"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tint fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('c2')">Ver detalle</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row my-3">
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Cárcamo 3</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard7"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tint fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('c3')">Ver detalle</button>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Presión 1</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard8"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tachometer-alt fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('p1')">Ver detalle</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row my-3">
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Presión 2</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard9"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tachometer-alt fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('p2')">Ver detalle</button>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 9rem;">
						<div class="row">
							<div class="col-9 ">
								<div class="card-body">
									<div class=" mb-1">Presión 3</div>
									<p class="font-weight-bold mt-2 mb-0 mx-auto" id="dataCard10"></p>
								</div>
							</div>
							<div class="col-3 pl-0 py-4">
								<i class="fas fa-tachometer-alt fa-2x text-white"></i>
							</div>
						</div>
						<div class="row">
							<button class="btn btn-outline-light text-xs mx-auto" onclick="showSensorPanel('p3')">Ver detalle</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="panel" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl border-0 rounded-0">
			<div class="modal-content p-4 border-0 rounded-0" id="sensorPanel">
				...
			</div>
		</div>
	</div>
@endsection
@section('scripts')
<script src="{{url('lib/moment.min.js')}}"></script>
<script src="{{url('lib/daterangepicker/daterangepicker.min.js')}}"></script>
	<script src="{{url('lib/chartjs.min.js')}}"></script>
	<script src="{{url('lib/daterangepicker/daterangepicker.min.js')}}"></script>
	<script>
		from=moment().startOf('week');
		to=moment();
		function updateData(from,to){
			$.ajax({
				type:'GET',
				url:'/data/get',
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				data:{
					'from':from.format('YYYY-MM-DD HH:mm:ss'),
					'to':to.format('YYYY-MM-DD HH:mm:ss'),
					'device':'{{$device->sigfox_id}}',
					'toJson':true,
				},
				success: function(response){
					//console.log(response);
					data=JSON.parse(response);
					//updating cards
					$('#dataCard1').html(data['sytesa_data'][0]['mag']);
					$('#dataCard2').html(data['sytesa_data'][0]['pal']);
					$('#dataCard3').html(data['sytesa_data'][0]['temperature']);
					$('#dataCard4').html(gateStatus[data['sytesa_data'][0]['gate']]);
					$('#dataCard5').html(levelStatus[data['sytesa_data'][0]['c1']]);
					$('#dataCard6').html(levelStatus[data['sytesa_data'][0]['c2']]);
					$('#dataCard7').html(levelStatus[data['sytesa_data'][0]['c3']]);
					$('#dataCard8').html(data['sytesa_data'][0]['p1']);
					$('#dataCard9').html(data['sytesa_data'][0]['p2']);
					$('#dataCard10').html(data['sytesa_data'][0]['p3']);
				},
				error: function(xhr){
					console.log(xhr.responseText);
				}
			});
		}
		$(document).ready(function(){
			updateData(from,to);
		});

		function showSensorPanel(sensor){
			$('#panel').modal('handleUpdate');
			$('#panel').modal('show');
			$('#sensorPanel').html('<div id="sensorPanel"><div class="spinner-border mx-auto" role="status"><span class="sr-only">Loading...</span></div><div>');
			$('#sensorPanel').load('/devices/{{$device->sigfox_id}}/panel/'+sensor);
			
		}

		gateStatus={
			0:'cerrado',
			1:'abierto',
		};
		levelStatus={
			0:'bajo',
			1:'medio',
			2:'medio',
			3:'alto',
		}
	</script>
@endsection