@extends('layouts.app')

{{-- @php
	$nCols=3;
	$supervisors=$region->supervisors()->get();
@endphp --}}

@section('navContent')
	<li class="nav-item mr-5">
		<form class="form-inline">
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="regionsSelect">
				<option value="0">cargando regiones</option>
			</select>
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="supervisorsSelect">
				<option value="0">Selecciona supervisor</option>
			</select>
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="plantsSelect">
				<option value="0">Selecciona planta</option>
			</select>
		</form>
	</li>
@endsection


@section('content')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent">
		<li class="breadcrumb-item"><a href="/main">main</a></li>
		<li class="breadcrumb-item active" aria-current="page">{{$region->name}}</li>
	</ol>
</nav>

<div class="row">
	<div class="col-8" id="cards">

	</div>
	<div class="col-4">

	</div>
</div>
<div class="row">
	<div class="col-lg-8">
		<div class="row" id="dataCards">
		
		</div>
	</div>
	<div class="col-lg-4">
		<div class="card rounded-0 border-0 shadow">
			<div class="card-body">
				<h5 class="card-title">Info</h5>
				<p class="mt-3 text-center text-muted">
					{{$region->info}}
				</p>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
	nCols=2
	$(document).ready(function(){
		$('#regionsSelect').change(function(){
			if($('#regionsSelect').val()!=0){
				location.href="/main/region/"+regionsArr[$('#regionsSelect').val()].name;
			}
		});
		$('#supervisorsSelect').change(function(){
			if($('#supervisorsSelect').val()!=0){
				location.href="/main/region/"+regionsArr[$('#regionsSelect').val()].name+"/supervisor/"+supervisorsArr[$('#supervisorsSelect').val()].userName;
			}
		});
		$('#dataCards').html(''
		// +'<div class="row">'
		+'	<div class="col-12 text-center align-middle">'
		+'		<p class="text-center lead mx-auto my-0"><div	style="width: 3rem; height: 3rem;" class="spinner-grow text-secondary my-5 align-middle" role="status"></div></p>'
		+'		<p class="text-center lead mx-auto my-0">Cargando</p>'
		+'	</div>'
		// +'</div>'
		);
		$.ajax({
			type:'GET',
			url:'/region/{{$region->name}}',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'with_supervisors':true,
				'with_supervisors_plants':true,
			},
			success: function(response){
				$('#dataCards').html('');
				console.log(response);
				data=JSON.parse(response);
				row=$('#dataCards');
				$.each(data.region.supervisors,function(i,supervisor){
					plantsList='';
					$.each(supervisor.plants,function(i,plant){
						plantsList=plantsList+'<li><a href="/main/region/{{$region->name}}/supervisor/'+supervisor.userName+'/planta/'+plant.name+'" class="btn btn-link">'+plant.name+'</a></li>';
					});
					col=$('<div class="col-lg-'+12/nCols+'">');
					card=$('<div class="card rounded-0 border-0 shadow my-3">').html(''
						+'	<div class="card-body">'
						+'		<h5 class="card-title"><i class="fas fa-user mr-2"></i>'+supervisor.userName+'</h5>'
						+'		<ul class="list-group list-group-flush">'
						+'			<li class="list-group-item list-group-item list-group-item-action">Nombre: '+supervisor.name+'</span></li>'
						+'			<li class="list-group-item list-group-item list-group-item-action">Apellido: '+supervisor.lastName+'</span></li>'
						+'			<li class="list-group-item list-group-item list-group-item-action">Email: '+supervisor.email+'</span></li>'
						+'			<li class="list-group-item list-group-item list-group-item-action">'
						+'				<div class="accordion" id="acordion-supervisor'+supervisor.id+'">'
						+'					<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse-supervisor-'+supervisor.id+'" aria-expanded="true" aria-controls="collapse-region-'+region.id+'">'
						+'						<span class="badge badge-primary badge-pill">'+supervisor.plants.length+'</span> Plantas'
						+'					</button>'
						+'					<div id="collapse-supervisor-'+supervisor.id+'" class="collapse" aria-labelledby="headingOne" data-parent="#acordion-supervisor'+supervisor.id+'">'
						+'						<div class="card-body">'
						+							'<ul class="list-unstyled">'+plantsList+'</ul>'
						+'						</div>'
						+'					</div>'
						+'				</div>'
						+'			</li>'
						+'		</ul>'
						+'		<a href="/main/region/{{$region->name}}/supervisor/'+supervisor.userName+'" class="btn btn-outline-dark btn-block rounded-pill mt-3 mb-2">Ver supervisor</a>'
						+'	</div>'
					);
					
					col.append(card);
					row.append(col);
				});
			}
		});
	});

</script>
@endsection