@extends('layouts.app')

@section('navContent')
	<li class="nav-item mx-3">
		<select class="form-control" disabled name="" id="regionsSelect">
			<option value="0">cargando regiones</option>
		</select>
	</li>
	<li class="nav-item mx-3">
		<select class="form-control" disabled name="" id="supervisorsSelect">
			<option value="0">Selecciona supervisor</option>
		</select>
	</li>
	<li class="nav-item mx-3">
		<select class="form-control" disabled name="" id="plantsSelect">
			<option value="0">Selecciona planta</option>
		</select>
	</li>
@endsection

@section('content')
@if (Auth::check())
@php
	$regions=App\Region::all();
@endphp

@if (Auth::check())

@endif

<div class="row">
	@for ($i = 0; $i < sizeof($regions); $i++)
	<div class="col-md-3">
		<div class="card rounded-0 border-0 shadow">
			<div class="card-body">
				<h5 class="card-title">{{$regions[$i]->name}}</h5>
				@php
				$supervisors=$regions[$i]->supervisors()->get();
				$plants=App\Plant::where('region_id',$regions[$i]->id)->get();
				@endphp
				<ul class="list-group list-group-flush">
					<li class="list-group-item list-group-item list-group-item-action"><i class="fas fa-user mr-2"></i>Supervisores: <span class="badge badge-primary badge-pill">{{sizeof($supervisors)}}</span></li>
					<li class="list-group-item list-group-item list-group-item-action"><i class="fas fa-tint mr-2"></i>Plantas: <span class="badge badge-primary badge-pill">{{sizeof($plants)}}</span></li>
				</ul>
			</div>
		</div>
	</div>
	@endfor
</div>
	
	
<div class="row my-3">
	<div class="col-md-6">
		<div class="card rounded-0 border-0 shadow">
			<div class="card-body">
				<h5 class="card-title">Mapa de regiones</h5>
				<img class="mt-n3" src="{{asset('img/elmapa-mx.png')}}" alt="">
				{{-- <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d7571191.360786669!2d-104.1210977!3d22.0927513!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2smx!4v1562706569163!5m2!1ses!2smx" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Info</h5>
				<canvas id="chart"></canvas>
			</div>
		</div>
	</div>
</div>
	
@else
	<div class="row">
		<div class="col-lg-6">
			<center>
				<img src="{{asset('img/057.jpg')}}" class="mx-automl-6" style="height: 25rem;" alt="">
			</center>
		</div>
		<div class="col-lg-6 " style="padding:15px 50px 0 0;margin: auto">
				<strong style="font-size: 25px">SYTESA</strong> es una empresa 100% mexicana que contribuye al cuidado del medio ambiente poniendo en manos de nuestros clientes la mejor tecnología, para lograr la sustentabilidad económica, ecológica y operativa del ciclo del agua. Nos enfocamos en brindar la mejor alternativa para lograr el reúso de agua.
		</div>
	</div>
	<div class="row mt-5 px-5">
		<div class="col-4">
			<center>
				<img src="https://sytesa.com.mx/wp-content/uploads/2016/01/01_drop.svg" class="vc_single_image-img attachment-full" style="height: 5rem"alt="">
				<br><h6 class="mt-4"AHORRAMOS<br>AGUA POTABLE</h6>
			</center>
			</div>
		<div class="col-4">
			<center>
				<img src="https://sytesa.com.mx/wp-content/uploads/2016/01/02_pipe.svg" class="vc_single_image-img attachment-full" style="height: 5rem"alt="">
				<br><h6 class="mt-4">DISMINUIMOS LA DESCARGA A DRENAJE</h6>
			</center>
			</div>
		<div class="col-4">
			<center>
				<img src="https://sytesa.com.mx/wp-content/uploads/2016/01/03_nom.svg" class="vc_single_image-img attachment-full" style="height: 5rem"alt="">
				<br><h6 class="mt-4">CUMPLIMOS NORMAS MEXICANAS</h6>
			</center>
		</div>
	</div>
@endif
@endsection
@section('scripts')
	<script src="{{asset('lib\chartjs.min.js')}}"></script>
	<script>
		@if (Auth::check())
		var oilCanvas = document.getElementById("chart");


var oilData = {
    labels: [
        "Region 1",
        "Region 2",
        "Region 3",
        "Region 4",
        "Region 5",
    ],
    datasets: [
        {
            data: [133.3, 86.2, 52.2, 51.2, 50.2],
            backgroundColor: [
                "#7B1FA2",
                "#448AFF",
                "#3F51B5",
                "#009688",
                "#00BCD4"
            ]
        }]
};

var pieChart = new Chart(oilCanvas, {
  type: 'pie',
  data: oilData
});
		@else
			
		@endif
	</script>
@endsection