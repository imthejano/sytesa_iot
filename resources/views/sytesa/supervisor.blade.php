@extends('layouts.app')

@section('navContent')
	<li class="nav-item mr-5">
		<form class="form-inline">
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="regionsSelect">
				<option value="0">cargando regiones</option>
			</select>
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="supervisorsSelect">
				<option value="0">Selecciona supervisor</option>
			</select>
			<select class="form-control my-2 my-lg-0 mx-0 mx-lg-3" disabled id="plantsSelect">
				<option value="0">Selecciona planta</option>
			</select>
		</form>
	</li>
@endsection


@section('content')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent">
		<li class="breadcrumb-item"><a href="/main">main</a></li>
		<li class="breadcrumb-item"><a href="/main/region/{{$region->name}}">{{$region->name}}</a></li>
		<li class="breadcrumb-item active" aria-current="page">{{$supervisor->userName}}</li>
	</ol>
</nav>

<div class="row">
	<div class="col-lg-8">
		<div class="row" id="dataCards">
		
		</div>
	</div>
	<div class="col-lg-4">
		<div class="card rounded-0 border-0 shadow">
			<div class="card-body">
				<h5 class="card-title">Info</h5>
				<p class="mt-3 text-center text-muted">
					{{$supervisor->info}}
				</p>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
	nCols=2;
	$('#regionsSelect').change(function(){
		if($('#regionsSelect').val()!=0){
			location.href="/main/region/"+regionsArr[$('#regionsSelect').val()].name;
		}
	});
	$('#supervisorsSelect').change(function(){
		if($('#supervisorsSelect').val()!=0){
			location.href="/main/region/"+regionsArr[$('#regionsSelect').val()].name+"/supervisor/"+supervisorsArr[$('#supervisorsSelect').val()].userName;
		}
	});
	$('#plantsSelect').change(function(){
		if($('#plantsSelect').val()!=0){
			location.href="/main/region/"+regionsArr[$('#regionsSelect').val()].name+"/supervisor/"+supervisorsArr[$('#supervisorsSelect').val()].userName+'/planta/'+plantsArr[$('#plantsSelect').val()].name;							
		}
	});

	$(document).ready(function () {
		$('#dataCards').html(''
		// +'<div class="row">'
		+'	<div class="col-12 text-center align-middle">'
		+'		<p class="text-center lead mx-auto my-0"><div	style="width: 3rem; height: 3rem;" class="spinner-grow text-secondary my-5 align-middle" role="status"></div></p>'
		+'		<p class="text-center lead mx-auto my-0">Cargando</p>'
		+'	</div>'
		// +'</div>'
		);
		$.ajax({
			type:'GET',
			url:'/supervisor/{{$supervisor->id}}',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:{
				'with_plants':true,
				'with_plants_region':true,
				'toJson':true,
			},
			success: function(response){
				$('#dataCards').html('');
				console.log(response);
				data=JSON.parse(response);
				row=$('#dataCards');
				$.each(data.supervisor.plants,function(i,plant){
					plantsList='';
					col=$('<div class="col-lg-'+12/nCols+'">');
					card=$('<div class="card rounded-0 border-0 shadow my-3">').html(''
						+'	<div class="card-body">'
						+'		<h5 class="card-title"><i class="fas fa-user mr-2"></i>'+plant.name+'</h5>'
						+'		<ul class="list-group list-group-flush">'
						+'			<li class="list-group-item list-group-item list-group-item-action">Direccion: '+plant.address+'</span></li>'
						+'			<li class="list-group-item list-group-item list-group-item-action">'
						+'				<div class="accordion" id="acordion-plant'+plant.id+'">'
						+'					<button class="btn btn-link text-dark" type="button" data-toggle="collapse" data-target="#collapse-plant-'+plant.id+'" aria-expanded="true" aria-controls="collapse-plant-'+plant.id+'">'
						+'						<span class="fa fa-map mx-2"></span>Ver mapa'
						+'					</button>'
						+'					<div id="collapse-plant-'+plant.id+'" class="collapse" aria-labelledby="headingOne" data-parent="#acordion-plant'+plant.id+'">'
						+'						<div class="map-responsive">'
						+							'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d'+plant.longitude+'!2d'+plant.latitude+'!3d'+plant.latitude+'!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d202552e587387%3A0xe9a8d1997a7478d8!2sSoluciones+Y+Tratamiento+Ecol%C3%B3gico%2C+SAPI+De+CV!5e0!3m2!1ses!2smx!4v1563229782838!5m2!1ses!2smx"  frameborder="0" style="border:0" allowfullscreen></iframe>'
						+'						</div>'
						+'					</div>'
						+'				</div>'
						+'			</li>'
						+'		</ul>'
						+'		<a href="/main/region/{{$region->name}}/supervisor/'+supervisor.userName+'/planta/'+plant.name+'" class="btn btn-outline-dark btn-block rounded-pill mt-3 mb-2">Ir a planta</a>'
						+'	</div>'
					);
					
					col.append(card);
					row.append(col);
				});
			}
		});
	});
</script>
@endsection