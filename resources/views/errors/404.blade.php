@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-4">
		<h1 class="display-1 text-secondary my-3">Error 404</h1>
		<h4 class="display-4 text-secondary my-3">Recurso no encontrado</h4>
		@if (isset($message))
			<h6 class="text-secondary">{{$message}}</h6>
		@endif
		<a href="javascript:window.history.back();" class="my-4 btn btn-outline-info btn-lg btn-block">Volver</a>
	</div>
	<div class="col-8" >
			<img  src="{{asset('img/404.png')}}" style="height: 80%"  alt="">
	</div>
</div>
@endsection
