<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::get('/regions/get','RegionController@get');
// Route::get('/region/{region}','RegionController@show');

// Route::get('/supervisors/get','SupervisorController@get');
// Route::get('/supervisor/{supervisor}','SupervisorController@show');

// Route::get('/data/get','DataController@get');

// Route::post('/device/store','DeviceController@store');
// Route::put('/device/activate','DeviceController@activate');

// Route::put('/plant/settings/update','PlanSettingsController@update');

// Route::get('data/getString','DataController@getString');
