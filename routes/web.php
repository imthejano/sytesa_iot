<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');    
});
Route::get('/index',function(){
	return view('sytesa.index');
});

Route::get('/main/region/{region}/supervisor/{supervisor}',function($region,$supervisor){
	return view('sytesa.supervisor');
});
Route::get('/main/region/{region}/supervisor/{supervisor}/planta/{plant}',function($region,$supervisor,$plant){
	return view('sytesa.plant')->with('plant',$plant);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/web/access','Auth\AuthController@authenticate');


Route::get('/devices/{device}/sensor/{sensor}',function($device,$sensor){
	$device=App\Device::where('sigfox_id',$device)->first();
	return view('sensors.panel')->with('device',$device)->with('sensor',$sensor);
});
Route::get('/devices/{device}/panel/mag',function($device){
	$device=App\Device::where('sigfox_id',$device)->first();
	if($device!=null){
		$plant=App\Plant::where('device_id',$device->sigfox_id)->first();
		return view('sensors.mag')->with('device',$device)->with('plant',$plant);
	}else{
		abort(404);
	}
});
Route::get('/devices/{device}/panel/pal',function($device){
	$device=App\Device::where('sigfox_id',$device)->first();
	if($device!=null){
		$plant=App\Plant::where('device_id',$device->sigfox_id)->first();
		return view('sensors.pal')->with('device',$device)->with('plant',$plant);
	}else{
		abort(404);
	}
});



Route::get('/main/region/{region}/supervisor/{supervisor}',function($region,$supervisor){
	$region=App\Region::where('name',$region)->first();
	if($region!=null){
		$supervisor=App\Supervisor::where('userName',$supervisor)->first();
		if($supervisor!=null){
			return view('sytesa.supervisor')->with('region',$region)->with('supervisor',$supervisor);
		}else{
			abort(404);
		}
	}else {
		abort(404);
	}
});

Route::get('/main/region/{region}',function($region){
	$region=App\Region::where('name',$region)->first();
	if($region!=null){
		return view('sytesa.region')->with('region',$region);
	}else {
		abort(404)->with('message','Region no encontrada');
	}
});

Route::get('/main',function(){
	return view('sytesa.main');
});



//data
Route::get('/regions/get','RegionController@get');
Route::get('/region/{region}','RegionController@show');

Route::get('/supervisors/get','SupervisorController@get');
Route::get('/supervisor/{supervisor}','SupervisorController@show');

Route::get('/data/get','DataController@get');

Route::post('/device/store','DeviceController@store');
Route::put('/device/activate','DeviceController@activate');

Route::put('/plant/settings/update','PlanSettingsController@update');

Route::get('data/getString','DataController@getString');




//parseador
Route::post('/parser','ParserController@parseAndStore');